#include <stdio.h>
#include <windows.h>

#include "TSFuncs.hpp"
#include "TSVector.hpp"

/* Some variables we want to expose to TorqueScript */
const char *gMyStrVar;
signed int gMyIntVar;
float gMyFloatVar;
bool gMyBoolVar;
Vector<signed int> gMyIntVecVar;
Vector<float> gMyFloatVecVar;

const char *TestStringCB(ADDR obj, signed int argc, const char *argv[])
{
	/* Print all passed in arguments to the console */
	for (signed int i = 0; i < argc; ++i)
		BlPrintf("TestStringCB: argv[%d] = %s", i, argv[i]);

	/* Gets the value of the TorqueScript global var $TestTSFuncs::varTest
	   and prints it to the console */
	const char *strVar = tsf_GetVar("$TestTSFuncs::varTest");
	BlPrintf("$TestTSFuncs::varTest = %s", strVar);

	return gMyStrVar;
}

int TestIntCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestIntCB: argv[1] = %s", argv[1]);

	/* Print all values in the vector to the console */
	Vector<signed int>::iterator iter;
	for (iter = gMyIntVecVar.begin(); iter != gMyIntVecVar.end(); ++iter)
		BlPrintf("%d", *iter);

	return gMyIntVar;
}

float TestFloatCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestFloatCB: argv[1] = %s", argv[1]);

	/* Print all values in the vector to the console */
	Vector<float>::iterator iter;
	for (iter = gMyFloatVecVar.begin(); iter != gMyFloatVecVar.end(); ++iter)
		BlPrintf("%g", *iter);

	return gMyFloatVar;
}

void TestVoidCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestVoidCB: argv[1] = %s", argv[1]);
}

bool TestBoolCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestBoolCB: argv[1] = %s", argv[1]);
	return gMyBoolVar;
}

/* To call this function, instantiate a ScriptObject 'so' and call so.TestObjectCB(); */
/* This function also prints the value of so.TestObjectField and sets it to a value */
int TestObjectCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestObjectCB: %s %s %s", argv[0], argv[1], argv[2]);

	/* Gets the value of object's TorqueScript field TestObjectField.
	   The last argument, NULL, is for array index */
	const char *value = tsf_GetDataField(obj, "TestObjectField", NULL);
	if (value)
		BlPrintf("TestObjectField = %s", value);

	/* Sets the TestObjectField to a string value.
	   The third argument, NULL, is for array index */
	tsf_SetDataField(obj, "TestObjectField", NULL, "this is a test");

	return *(unsigned int *)(obj + 32);
}

/* This function can only be called once you do activatePackage("myPackage"); in Blockland */
int TestIntPackageCB(ADDR obj, signed int argc, const char *argv[])
{
	BlPrintf("TestIntPackageCB: argv[1] = %s", argv[1]);
	return 25351;
}

/* Called when the DLL is injected
   Returns true to indicate success
   Returns false to indicate failure */
bool init()
{
	BlInit; /* Initializes TSHooks */

	/* Bail if TSFuncs can't load */
	if (!tsf_InitInternal())
		return false;

	/* Expose our functions to TorqueScript */
	tsf_AddConsoleFunc(NULL,        NULL,           "TestStringCB",     TestStringCB,     "Some string usage",  1, 5); /* Normal functions require a minarg of 1 */
	tsf_AddConsoleFunc(NULL,        NULL,           "TestIntCB",        TestIntCB,        "Some int usage",     1, 2);
	tsf_AddConsoleFunc(NULL,        NULL,           "TestFloatCB",      TestFloatCB,      "Some float usage",   1, 2);
	tsf_AddConsoleFunc(NULL,        NULL,           "TestVoidCB",       TestVoidCB,       "Some void usage",    1, 2);
	tsf_AddConsoleFunc(NULL,        NULL,           "TestBoolCB",       TestBoolCB,       "Some bool usage",    1, 2);
	tsf_AddConsoleFunc(NULL,        "ScriptObject", "TestObjectCB",     TestObjectCB,     "Some obj usage",     2, 3); /* Object functions require a minarg of 2 */
	tsf_AddConsoleFunc("myPackage", NULL,           "TestIntPackageCB", TestIntPackageCB, "Some package usage", 1, 2);

	/* The same as using eval("code"); in TorqueScript */
	tsf_Eval("echo(\"This was printed via tsf_Eval from TestTSFuncs\");");
	tsf_Evalf("echo(\"%s\", \": \", %d);", "This was printed via tsf_Evalf from TestTSFuncs", 5);

	tsf_BlCon__executef(2, "echo", "This was printed via tsf_BlCon__executef from TestTSFuncs");

	const char *realTime = tsf_BlCon__executef(1, "getRealTime");
	BlPrintf("Result of tsf_BlCon__executef(1, \"getRealTime\") -> %s", realTime);

	ADDR playGui = tsf_FindObject("PlayGui");
	if (playGui) {
		char buf[16];
		snprintf(buf, 16, "%d", *(unsigned int *)(playGui + 32));
		tsf_BlCon__executef(3, "echo", "PlayGui ID: ", buf);
	}
	else
		BlPrintf("PlayGui could not be found! (expected if loaded via BlocklandLoader)");

	/* Set our variables to some default value */
	gMyStrVar = "This is a test";
	gMyIntVar = 517;
	gMyFloatVar = 3.14159*2;
	gMyBoolVar = true;

	gMyIntVecVar.push_back(1);
	gMyIntVecVar.push_back(2);
	gMyIntVecVar.push_back(3);

	gMyFloatVecVar.push_back(3.14159);
	gMyFloatVecVar.push_back(3.14159*2);
	gMyFloatVecVar.push_back(2.71828);

	/* Expose them to TorqueScript */
	tsf_AddVar("TestTSFuncs::myStrVar",      &gMyStrVar);
	tsf_AddVar("TestTSFuncs::myIntVar",      &gMyIntVar);
	tsf_AddVar("TestTSFuncs::myFloatVar",    &gMyFloatVar);
	tsf_AddVar("TestTSFuncs::myBoolVar",     &gMyBoolVar);
	tsf_AddVar("TestTSFuncs::myIntVecVar",   &gMyIntVecVar);
	tsf_AddVar("TestTSFuncs::myFloatVecVar", &gMyFloatVecVar);

	tsf_Eval("$TestTSFuncs::varTest = \"some value\";");

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

/* Called when the DLL is ejected
   Returns true to indicate success */
bool deinit()
{
	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

/* Entry point */
bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
