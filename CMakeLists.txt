cmake_minimum_required(VERSION 3.6)

project(tsfuncs LANGUAGES CXX)

set(TSFUNCS_MAJOR_VERSION 1)
set(TSFUNCS_MINOR_VERSION 0)
set(TSFUNCS_PATCH_VERSION 1)
set(TSFUNCS_VERSION ${TSFUNCS_MAJOR_VERSION}.${TSFUNCS_MINOR_VERSION}.${TSFUNCS_PATCH_VERSION})

set(CMAKE_CXX_FLAGS "-static -static-libgcc -static-libstdc++ -Wall -Werror")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2 -DTSFUNCS_DEBUG=false")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -g -Og")

include(ExternalProject)
include(CMakePackageConfigHelpers)

set(CMAKE_CONFIGURATION_TYPES "Debug;Release" CACHE STRING "" FORCE)

if (NOT CMAKE_BUILD_TYPE MATCHES Release AND NOT CMAKE_BUILD_TYPE MATCHES Debug)
	set(CMAKE_BUILD_TYPE Release)
endif ()

if (DEFINED CMAKE_TOOLCHAIN_FILE AND NOT "${CMAKE_TOOLCHAIN_FILE}" STREQUAL "")
	message(STATUS "Toolchain: ${CMAKE_TOOLCHAIN_FILE}")
else ()
	message(STATUS "Toolchain: (None)")
endif ()
message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
message(STATUS "Install prefix: \"${CMAKE_INSTALL_PREFIX}\"")

add_library(${CMAKE_PROJECT_NAME} STATIC
	"src/TSFuncs.cpp"
	"src/TSHooks.cpp"
	"src/TSVector.cpp"
)

ExternalProject_Add(minhook_project
	GIT_REPOSITORY "https://github.com/TsudaKageyu/minhook"
	GIT_TAG "423d1e45af2ed2719a5c31e990e935ef301ed9c3"
	GIT_SHALLOW OFF
	GIT_PROGRESS true
	CMAKE_ARGS
		-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_INSTALL_PREFIX}
		-DCMAKE_TOOLCHAIN_FILE:PATH=${CMAKE_TOOLCHAIN_FILE}
		-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
)

add_dependencies(${CMAKE_PROJECT_NAME} minhook_project)

target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC
	$<BUILD_INTERFACE:${CMAKE_INSTALL_PREFIX}/include>
	$<INSTALL_INTERFACE:include>
)

link_directories(${CMAKE_PROJECT_NAME} PUBLIC
	$<BUILD_INTERFACE:${CMAKE_INSTALL_PREFIX}/lib>
	$<INSTALL_INTERFACE:lib>
)

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES DEBUG_POSTFIX "d")

configure_package_config_file(
	"tsfuncs-config.cmake.in"
	"tsfuncs-config.cmake"
	INSTALL_DESTINATION
		${CMAKE_INSTALL_PREFIX}
)

write_basic_package_version_file(
	"tsfuncs-config-version.cmake"
	VERSION
		${TSFUNCS_VERSION}
	COMPATIBILITY
		AnyNewerVersion
)

install(
	FILES
		"${CMAKE_CURRENT_BINARY_DIR}/tsfuncs-config.cmake"
		"${CMAKE_CURRENT_BINARY_DIR}/tsfuncs-config-version.cmake"
	DESTINATION
		${CMAKE_INSTALL_PREFIX}
)

install(TARGETS ${CMAKE_PROJECT_NAME}
	EXPORT tsfuncs-targets
	DESTINATION ${CMAKE_INSTALL_PREFIX}
	RUNTIME DESTINATION "${CMAKE_INSTALL_PREFIX}/bin"
	ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
	LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
)

install(
	EXPORT
		tsfuncs-targets
	DESTINATION
		${CMAKE_INSTALL_PREFIX}
)

install(
	FILES
		"src/TSFuncs.hpp"
		"src/TSHooks.hpp"
		"src/TSVector.hpp"
	DESTINATION
		"${CMAKE_INSTALL_PREFIX}/include"
)
